package books.com.books.Controller;

import books.com.books.models.Book;
import books.com.books.service.BooksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.relational.core.sql.Literal;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class BooksController {


    @Autowired
    BooksService service;

   @PostMapping("/book")
     Book create(@RequestBody Book book){
       return service.save(book);
   }

   @GetMapping("/book")
    public Iterable<Book> get(){
       return service.findAll();
   }
   @GetMapping("/book/{id}")
    public Optional<Book> getById(@PathVariable Integer id){
       return service.findById(id);
   }
    @DeleteMapping("/book/{id}")
    public void Delete(@PathVariable Integer id){
         service.deleteById(id);
    }

    @PutMapping("/book/{id}")
    Book replaceEmployee(@RequestBody Book newEmployee, @PathVariable Integer id) {
        return service.findById(id)
                .map(employee -> {
                    employee.setId(newEmployee.getId());
                    employee.setStores(newEmployee.getStores());
                    employee.setPrice(newEmployee.getPrice());
                    employee.setCopies(newEmployee.getCopies());
                    employee.setBookName(newEmployee.getBookName());
                    employee.setAuthor(newEmployee.getAuthor());
                    return service.save(employee);
                })
                .orElseGet(() -> {
                    newEmployee.setId(id);
                    return service.save(newEmployee);
                });
    }

}
