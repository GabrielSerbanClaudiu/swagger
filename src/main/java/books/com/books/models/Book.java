package books.com.books.models;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.text.DecimalFormat;
import java.util.List;

@Entity public class Book {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Integer id;

        @JsonProperty("book-name")
        private String bookName;
        @JsonProperty("book-price")
        private String price;
        @JsonProperty("copies")
        private Integer copies;

        @Embedded
        @JsonProperty("author")
         Author author;

        @OneToMany(cascade = CascadeType.ALL)
         List<CityStore> stores;

        public Book(){

        }

    public List<CityStore> getStores() {
        return stores;
    }

    public void setStores(List<CityStore> stores) {
        this.stores = stores;
    }

    public Book(String bookName, String price, Integer copies, Author author, List<CityStore> stores) {
        this.bookName = bookName;
        this.price = price;
        this.copies = copies;
        this.author = author;
        this.stores = stores;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getPrice() {

        return price;
    }

    public void setPrice(String price) {
        this.price =price;
    }

    public int getCopies() {
        return copies;
    }

    public void setCopies(Integer copies) {
        this.copies = copies;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<CityStore> GetsoldAt() {
        return stores;
    }

    public void SetsoldAt(List<CityStore> store) {
        this.stores = store;
    }
}
